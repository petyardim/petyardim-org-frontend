FROM node:18.9.0-alpine AS deps
WORKDIR /app
COPY package.json ./
RUN yarn install 
COPY . .


FROM node:18.9.0-alpine AS builder
WORKDIR /app
COPY --from=deps /app ./
RUN yarn build


FROM node:18.9.0-alpine AS runner
WORKDIR /app
COPY --from=builder /app/public ./public
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/.next ./.next
EXPOSE 3000
CMD ["yarn", "start"]