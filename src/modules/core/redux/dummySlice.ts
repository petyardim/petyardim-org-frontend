import { createSlice } from "@reduxjs/toolkit";

const dummySlice = createSlice({
    name: "dummy",
    reducers: {},
    initialState: {
        dummystate: "Hello World",
    },
});

export default dummySlice.reducer;
