import "@/assets/styles/globals.css";
import type { AppProps } from "next/app";
import { Provider } from "react-redux";
import { wrapper } from "../src/modules/core/redux";
import { ConfigProvider } from "antd";
import "antd/dist/reset.css";

function MyApp({ Component, pageProps, ...rest }: AppProps) {
    const { store } = wrapper.useWrappedStore(rest);
    return (
        <Provider store={store}>
            <ConfigProvider>
                <Component {...pageProps} />
            </ConfigProvider>
        </Provider>
    );
}

export default MyApp;
