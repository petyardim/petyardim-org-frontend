import { Button } from "antd";
import type { NextPage } from "next";
import { useAppSelector, wrapper } from "../src/modules/core/redux";

const Home: NextPage = () => {
    const { dummystate } = useAppSelector(state => state.dummy);
    console.log(dummystate);
    return (
        <div className=" m-5">
            <Button className="bg-orange-400">Hello World</Button>
        </div>
    );
};

Home.getInitialProps = wrapper.getInitialPageProps(({}) => () => {
    return {
        dummy: "asd",
    };
});

export default Home;
